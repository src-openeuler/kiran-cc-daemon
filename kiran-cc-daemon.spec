Name: 		kiran-cc-daemon
Version:	2.6.1
Release:	24%{?dist}
Summary:	DBus daemon for Kiran Desktop

License:	MulanPSL-2.0
Source0:	%{name}-%{version}.tar.gz
Source1:	99_unikylin-mate-SettingsDaemon-plugins.gschema.override
Source2:	99_unikylin-mate-Marco-global-keybindings.gschema.override

Patch0000:	0000-fix-xsettings-set-default-icon-theme-to-Spring.patch
Patch0001:	0001-fix-xsettings-Fix-font-enlargement-on-4K-screen.patch
Patch0002:	0001-fix-media-keys-Fix-use-shortcut-key-with-shift.patch
Patch0003:	0003-fix-power-tray-Fixed-an-issue-where-the-laptop-tray-.patch
Patch0004:	0004-fix-keybinding-Fix-shortcut-grab-in-system-and-conf-.patch
Patch0005:	0005-fix-power-Fix-dpms-force-level-effective-while-state.patch
Patch0006:	0006-feature-appearance-Add-support-reset-default-font.patch
Patch0007:	0007-fix-keybidning-avoid-shift-to-participate-in-transla.patch
Patch0008:	0008-fix-appearance-Change-the-light-theme-name-to-Kiran-.patch
Patch0009:	0009-fix-power-Fix-coredump-while-log-print-in-power.patch
Patch0010:	0010-feature-CI-ref-new-CI-entry-point.patch
Patch0011:	0011-fix-audio-Fix-audio-stream-volume-retain-while-set-m.patch
Patch0012:	0012-fix-power-Fix-battery-charging-and-discharging-time.patch
Patch0013:	0013-fix-power-Fix-sys-backlight-set-only-in-notebook-wit.patch
Patch0014:	0014-fix-touchpad-Fix-touchpad-control-with-the-type-psmo.patch
Patch0015:	0015-fix-display-Fix-handling-Dbus-method-calls-returning.patch
Patch0016:	0016-fix-power-Fix-poweroff-action-while-button-event-dup.patch
Patch0017:	0017-feature-display-perform-adaptive-settings-when-conne.patch
Patch0018:	0018-fix-systeminfo-fix-the-graphical-and-network-hardwar.patch
Patch0019:	0019-fix-systeminfo-Remove-the-duplicate-graphical-and-ne.patch
Patch0020:	0020-fix-media-keys-action-The-shutdown-shortcut-cannot-b.patch
Patch0021:	0021-fix-media-keys-action-fix-compile-error.patch
Patch0022:	0022-feature-media-keys-add-shutdown-keybinding.patch
Patch0023:	0023-fix-Ungrab-Synchronize-requests-to-XServer-after-UnG.patch
Patch0024:	0024-fix-systeminfo-hardware-Fix-failure-to-get-disk-info.patch
Patch0025:	0025-fix-timedate-Fix-the-ntp-service-load-order-logic-er.patch
Patch0026:	0026-fix-systeminfo-fix-std-regex-can-not-run-in-gcc-vers.patch
Patch0027:	0027-feature-systeminfo-hardware-use-glibmm-s-regex-to-re.patch
Patch0028:	0028-fix-gnome-theme-Fix-the-issue-where-switching-betwee.patch
Patch0029:	0029-fix-gnome-Fix-crash-caused-by-the-previous-commit-du.patch
Patch0030:	0030-fix-passwd-enriching-pw_history-error-reporing.patch

BuildRequires:	cmake >= 3.2
BuildRequires:	pkgconfig(glibmm-2.4)
BuildRequires:	pkgconfig(giomm-2.4)
BuildRequires:	pkgconfig(gmodule-2.0)
BuildRequires:	pkgconfig(gio-unix-2.0)
BuildRequires:	pkgconfig(gtkmm-3.0)
BuildRequires:	pkgconfig(libxml++-2.6)
BuildRequires:	systemd-devel
BuildRequires:  libselinux-devel
BuildRequires:  gettext
BuildRequires:	gcc-c++
BuildRequires:	intltool
BuildRequires:	libX11-devel
BuildRequires:	xerces-c-devel
BuildRequires:	xsd
BuildRequires:	fontconfig-devel
BuildRequires:	jsoncpp-devel
BuildRequires:	cairomm-devel
BuildRequires:  kiran-log-gtk3-devel
BuildRequires:	fmt-devel >= 6.2.1

%if "%{?kylin_major_version}" == "3" && "%{?kylin_minor_version}" == "3" && "%{?kylin_sub_minor_version}" == "6"
BuildRequires:	python36-jinja2
%else
BuildRequires:	python%{python3_pkgversion}-jinja2
%endif

BuildRequires:	gdbus-codegen-glibmm
BuildRequires:	gtest-devel
BuildRequires:	libnotify-devel
BuildRequires:	pulseaudio-libs-devel
BuildRequires:	libgtop2-devel
BuildRequires:	libgudev-devel
BuildRequires:	cryptopp-devel
BuildRequires:	libwnck3-devel


%description
DBus daemon for Kiran Desktop


%package -n kiran-system-daemon
Summary:	System DBus daemon for Kiran Desktop
%{?systemd_requires}

%if 0%{?rhel} > 7 || 0%{?openEuler}
Requires:	dbus-daemon
%else
Requires:	dbus
%endif

Requires:	polkit
Requires:	kiran-cc-daemon-common
Requires:	util-linux
Requires:	pciutils
Requires:	libgtop2
Requires:	libgudev
Requires:	lshw
Requires:	passwd
Requires:	iso-codes

%if "%{?kylin}" == ""
Requires:	openeuler-lsb
%else
Requires:	%{_vendor}-lsb
%endif

%description -n kiran-system-daemon
System DBus daemon for Kiran Desktop

%package -n kiran-session-daemon
Summary:	Session DBus daemon for Kiran Desktop

%if 0%{?rhel} > 7 || 0%{?openEuler}
Requires:	dbus-daemon
%else
Requires:	dbus
%endif

Requires:	kiran-session-manager
Requires:	xorg-x11-server-utils
Requires:	kiran-cc-daemon-common
Requires:	upower
Requires:	pulseaudio
Requires:	libwnck3

%description -n kiran-session-daemon
Session DBus daemon for Kiran Desktop

%package common
Summary:	Common files for kiran-session-daemon and kiran-system-daemon

%description common
Common files for kiran-session-daemon and kiran-system-daemon

%package devel
Summary:	Development files for communicating with control center daemon

%description devel
Development files for communicating with control center daemon

%prep
%autosetup -p1

%build

%cmake

%cmake_build

%install
%cmake_install

install -D -m 0644 %SOURCE1 %{buildroot}%{_datadir}/glib-2.0/schemas/99_unikylin-mate-SettingsDaemon-plugins.gschema.override
install -D -m 0644 %SOURCE2 %{buildroot}%{_datadir}/glib-2.0/schemas/99_unikylin-mate-Marco-global-keybindings.gschema.override

%post -n kiran-system-daemon
%systemd_post kiran-system-daemon.service
systemctl enable kiran-system-daemon.service

%preun -n kiran-system-daemon
%systemd_preun kiran-system-daemon.service

%files -n kiran-system-daemon
%{_sysconfdir}/dbus-1/system.d/*.conf
%{_sysconfdir}/lightdm/kiran-greeter.conf
%config(noreplace) %{_sysconfdir}/kiran-cc-daemon/system/timedate/timedate.conf
%{_bindir}/kiran-system-daemon
%{_usr}/lib/systemd/system/*.service
%{_libdir}/kiran-cc-daemon/system/*.so
%{_libdir}/kiran-cc-daemon/system/plugin_options
%{_datadir}/dbus-1/system-services/*.service
%{_datadir}/locale/zh_CN/LC_MESSAGES/kiran-cc-daemon-timezones.mo
%{_datadir}/locale/bo_CN/LC_MESSAGES/kiran-cc-daemon-timezones.mo
%{_datadir}/locale/kk_KG/LC_MESSAGES/kiran-cc-daemon-timezones.mo
%{_datadir}/locale/kk_KZ/LC_MESSAGES/kiran-cc-daemon-timezones.mo
%{_datadir}/locale/mn_MN/LC_MESSAGES/kiran-cc-daemon-timezones.mo
%{_datadir}/locale/ug_CN/LC_MESSAGES/kiran-cc-daemon-timezones.mo
%{_datadir}/polkit-1/actions/com.kylinsec.Kiran.SystemDaemon*.policy
%{_datadir}/lightdm/lightdm.conf.d/99-kiran-greeter-login.conf

%files -n kiran-session-daemon
%{_sysconfdir}/xdg/autostart/kiran-session-daemon.desktop
%{_sysconfdir}/xdg/autostart/kiran-power-status-icon.desktop
%{_bindir}/kiran-session-daemon
%{_bindir}/kiran-power-backlight-helper
%{_bindir}/kiran-power-status-icon
%{_libdir}/kiran-cc-daemon/session/*.so
%{_libdir}/kiran-cc-daemon/session/plugin_options
%{_datadir}/glib-2.0/schemas/*.xml
%{_datadir}/dbus-1/services/*.service
%{_datadir}/icons/hicolor/scalable/apps/*.svg
%{_datadir}/locale/zh_CN/LC_MESSAGES/kiran-power-status-icon.mo
%{_datadir}/locale/bo_CN/LC_MESSAGES/kiran-power-status-icon.mo
%{_datadir}/locale/kk_KG/LC_MESSAGES/kiran-power-status-icon.mo
%{_datadir}/locale/kk_KZ/LC_MESSAGES/kiran-power-status-icon.mo
%{_datadir}/locale/mn_MN/LC_MESSAGES/kiran-power-status-icon.mo
%{_datadir}/locale/ug_CN/LC_MESSAGES/kiran-power-status-icon.mo
%{_datadir}/polkit-1/actions/com.kylinsec.Kiran.SessionDaemon*.policy
%dir %{_datadir}/kiran-cc-daemon/keybindings
%{_datadir}/kiran-cc-daemon/keybindings/*.xml
%{_datadir}/glib-2.0/schemas/99_unikylin-mate-SettingsDaemon-plugins.gschema.override
%{_datadir}/glib-2.0/schemas/99_unikylin-mate-Marco-global-keybindings.gschema.override

%files common
%{_includedir}/kiran-cc-daemon/error-i.h
%{_datadir}/locale/zh_CN/LC_MESSAGES/kiran-cc-daemon.mo
%{_datadir}/locale/bo_CN/LC_MESSAGES/kiran-cc-daemon.mo
%{_datadir}/locale/kk_KG/LC_MESSAGES/kiran-cc-daemon.mo
%{_datadir}/locale/kk_KZ/LC_MESSAGES/kiran-cc-daemon.mo
%{_datadir}/locale/mn_MN/LC_MESSAGES/kiran-cc-daemon.mo
%{_datadir}/locale/ug_CN/LC_MESSAGES/kiran-cc-daemon.mo

%files devel
%dir %{_includedir}/kiran-cc-daemon
%dir %{_includedir}/kiran-cc-daemon/kiran-system-daemon
%dir %{_includedir}/kiran-cc-daemon/kiran-session-daemon
%{_includedir}/kiran-cc-daemon/kiran-system-daemon/*-i.h
%{_includedir}/kiran-cc-daemon/kiran-session-daemon/*-i.h
%{_libdir}/pkgconfig/kiran-cc-daemon.pc

%changelog
* Sun Dec 08 2024 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.6.1-24
- KYOS-F: Decrease the priority of override file configurations(#52881)
- KYOS-F: Marking the configuration as not to be replaced
- KYOS-B: Enrichment of error messages in pw_history module when changing passwords(#52096)
- KYOS-B: Fix crash caused by the previous commit due to the existence of a schema but the absence of a color-schema (#35120)
- KYOS-B: Fix the issue where switching between light and dark themes does not work in Gnome 41 and later versions.(#35120)
- KYSO-B: Fix the ntp service load order logic error
- KYSO-B: Fix std::regex can not run in gcc version < 4.9(#48386)
- KYSO-F: Use glibmm's regex to replace glib's regex
- KYSO-B: Fix failure to get disk information in virtual machine(#46645)
- KYOS-B: Fixed the issue where Unglab was not executed in a timely manner, causing subsequent Xclient connections to XServer to fail
- KYOS-B: fix the graphical and network hardware info no display problem.(#37289)
- KYOS-B: Remove the duplicate graphical and network hardware info.
- KYOS-B: The shutdown shortcut cannot be found.(#44337)
- KYOS-F: add shutdown keybinding
- KYOS-B: perform adaptive settings when connection status or monitor uid is changed.(#37289)

* Wed Nov 20 2024 Funda Wang <fundawang@yeah.net> - 2.6.1-16
- adopt to new cmake macro

* Mon Apr 22 2024 meizhigang <meizhigang@kylinsec.com.cn> - 2.6.1-15
- KYOS-B: Fix poweroff action while button event duplicate once clicked (#28620)

* Thu Apr 18 2024 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.6.1-14
- KYOS-B: Fix handling Dbus method calls returning twice causing crashes(#35488)

* Wed Apr 17 2024 meizhigang <meizhigang@kylinsec.com.cn> - 2.6.1-12
- KYOS-B: Fix touchpad control with the type psmouse (#34878)

* Wed Apr 10 2024 meizhigang <meizhigang@kylinsec.com.cn> - 2.6.1-11
- KYOS-B: Add battery charging and discharging time (#28618)
- KYOS-B: Fix sys backlight set only in notebook with battery (#31917)

* Mon Apr 01 2024 meizhigang <meizhigang@kylinsec.com.cn> - 2.6.1-10
- KYOS-B: Change the light theme name to Kiran-white (#24747)
- KYOS-B: Fix coredump while log print in power (#31141)
- KYOS-B: ref new CI entry point
- KYOS-B: Fix audio stream volume retain while set mute (#33694)

* Wed Mar 20 2024 yinhongchang <yinhongchang@kylinsec.com.cn> - 2.6.1-9
- KYOS-B: openeuler and kylinsec replaced with macro definitions %{_vendor}

* Mon Mar 04 2024 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.6.1-8
- KYOS-B: avoid shift to participate in translator keycodes(#25039)

* Wed Feb 21 2024 meizhigang <meizhigang@kylinsec.com.cn> - 2.6.1-7
- KYOS-F: Add support reset default font(#20203)

* Wed Jan 31 2024 meizhigang <meizhigang@kylinsec.com.cn> - 2.6.1-6
- KYOS-B: Fix dpms force level effective while state changed (#24589)

* Tue Jan 30 2024 meizhigang <meizhigang.kylinsec.com.cn> - 2.6.1-5
- KYOS-B: Fix shortcut grab in system and conf saved in custom (#25039 #29448)

* Fri Jan 19 2024 yangfeng <yangfeng@kylinsec.com.cn> - 2.6.1-4
- KYOS-B: Fixed an issue where the laptop tray battery icon is not the same size as other icons. (#25531)

* Fri Jan 05 2024 meizhigang <meizhigang.kylinsec.com.cn> - 2.6.1-3
- KYOS-B: Fix font enlargement on 4K screen (#24084)
- KYOS-B: Fix use shortcut key with shift (#25039)

* Fri Dec 29 2023 yanglan <yanglan@kylinsec.com.cn> - 2.6.1-2.kb3
- rebuild for KiranUI-2.6-next

* Mon Dec 25 2023 yanglan <yanglan@kylinsec.com.cn> - 2.6.1-2.kb2
- rebuild for KiranUI-2.6-next

* Thu Dec 14 2023 yuanxing <yuanxing.kylinsec.com.cn> - 2.6.1-2
- KYOS-F: change the default icon theme to Spring

* Tue Dec 12 2023 meizhigang <meizhigang.kylinsec.com.cn> - 2.6.1-1
- rebuild for KiranUI-2.6-next
- KYOS-B: Add interface for get card list(#20195)

* Wed Nov 29 2023 meizhigang <meizhigang.kylinsec.com.cn> - 2.6.0-1
- rebuild for KiranUI-2.6-next

* Mon Sep 11 2023 longcheng <longcheng@kylinsec.com.cn> - 2.5.1-16.kb2
- KYOS-B: change BuildRequires in redhat 7 (#14460)

* Mon Jul 24 2023 kpkg <kpkg.kylinsec.com.cn> - 2.5.1-16.kb1
- rebuild for KiranUI-2.5-next

* Fri Jul 21 2023 meizhigang <meizhigang@kylinsec.com.cn> - 2.5.1-16
- KYOS-F: Add battery charging and discharging time (#9506)
- KYOS-F: Fix ci build error

* Mon Jul 10 2023 meizhigang <meizhigang@kylinsec.com.cn> - 2.5.1-15
- KYOS-F: Add capslock and numlock tips switch (#9379)

* Sat Jul 08 2023 tangjie02 <tangjie02@kylinsec.com.cn> - 2.5.1-13
- KYOS-F: Compatiable two power profile daemon which contains hadess and tuned.
- KYOS-B: Fix the problem that cannot match correct monitor when has same edid.

* Fri Jun 9 2023 wangtaozhi <wangtaozhi@kylinsec.com.cn> - 2.5.1-12
- KYOS-F: Adjusting the reading method of cpu information
- KYOS-F: Resolve program crash issue when exiting the program

* Tue May 30 2023 huangjiawen <huangjiawen@kylinsec.com.cn> - 2.5.1-11
- KYOS-F: Add adaptive screen change flag

* Tue May 30 2023 meizhigang <meizhigang@kylinsec.com.cn> - 2.5.1-10
- KYOS-F: Add support desktop extend key for GC

* Thu May 11 2023 wangtaozhi <wangtaozhi@kylinsec.com.cn> - 2.5.1-9
- KYOS-F: Fix identification issues in the control center

* Wed May 10 2023 tangjie02 <tangjie02@kylinsec.com.cn> - 2.5.1-8
- KYOS-F: Delete LockScreenWhenHibernate and LockScreenWhenSuspend functions and replace suspend/hibernate/shutdown api with SessionManager.

* Thu Apr 27 2023 tangjie02 <tangjie02@kylinsec.om.cn> - 2.5.1-7
- KYOS-F: Prior to select last ntp service in ntp-utils.d directory.

* Sun Apr 23 2023 kpkg <kpkg.kylinsec.com.cn> - 2.5.1-6
- rebuild for KY3.4-5-GC-KiranUI-2.5

* Sun Apr 23 2023 kpkg <kpkg.kylinsec.com.cn> - 2.5.1-5
- rebuild for KY3.4-5-GC-KiranUI-2.5

* Sun Apr 23 2023 wangyucheng <wangyucheng@kylinsec.om.cn> - 2.5.1-4
- KYOS-F: Conditional statement error

* Fri Apr 14 2023 meizhigang <meizhigang@kylinsec.com.cn> - 2.5.1-3
- KYOS-B: Add modifier lock window tips enable key with gsettings (#67766)
- KYOS-B: Fix coredump problem caused by nullpointer to string
- KYOS-B: Fix the type of return value in template

* Mon Apr 10 2023 wangyucheng <wangyucheng@kylinsec.om.cn> - 2.5.1-2
- KYOS-T: add some translation

* Wed Mar 29 2023 tangjie02 <tangjie02@kylinsec.com.cn> - 2.5.1-1
- KYOS-F: Add some power functions. (#68459)

* Fri Mar 24 2023 meizhigang <meizhigang@kylinsec.com.cn> - 2.5.0-1
- KYOS-F: init commit for v2.5 version.

